To run:

go run simpledb.go

The database can store not only ints; both its keys and its values are strings.

Maps seem to be appropriate in order to satisfy the performance requirements
since only one value can be associated with each key.

Statistics are also stored in a map from values to number of occurrences in the
database.

I chose the Go language solely out of pragmatism; we are using this language
currently in the distributed systems course that I am taking.