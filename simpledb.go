package main

import (
	"bufio"
	"container/list"
	"fmt"
	"os"
	"strings"
)

type SimpleDB struct {
	// the database
	db DBType
	// how many times each value occurs
	stats map[string]int64
	// supposed to be a stack of transactions
	transactions *list.List
}

type TransactionEntry struct {
	oldValue string
	didExist bool
}

var me SimpleDB

type DBType map[string]string
type TransactionMap map[string]TransactionEntry

type Transaction struct {
	entries TransactionMap
}

func (t *Transaction) Add(key string, transactionEntry *TransactionEntry) {
	_, isIn := t.entries[key]
	if !isIn {
		t.entries[key] = *transactionEntry
	}
}

/*	Get value from database
	key: value corresponding to this key to retrieve
*/
func Get(key string) string {
	value, isIn := me.db[key]
	if isIn {
		return value
	} else {
		return "NULL"
	}
}

func SetHelper(key string, value string, oldValue string, isIn bool) {
	if isIn {
		StatRemove(oldValue)
	}
	me.db[key] = value
	StatAdd(value)
}

func Set(key string, value string) string {
	oldValue, isIn := me.db[key]
	TransactionSet(key, oldValue, isIn)

	SetHelper(key, value, oldValue, isIn)
	return ""
}

func UnSetHelper(key string, oldValue string) {
	delete(me.db, key)
	StatRemove(oldValue)
}

/*	Removes an entry in the database
	key: key corresponding to entry to remove
	Will do nothing if key doesn't exist.
*/
func UnSet(key string) string {
	value, isIn := me.db[key]

	if isIn {
		TransactionSet(key, value, isIn)

		UnSetHelper(key, value)
	}
	return ""
}

/*	Returns the number of entries in the database that have specified value
	Returns 0 if the specified value isn't in the database
*/
func NumEqualTo(value string) string {
	return fmt.Sprintf("%d", me.stats[value])
}

/*	Update statistics (how many times value occurs in the
	database
	isRemove: whether to remove an occurrence the value
*/
func Stat(value string, isRemove bool) {
	howMany, isIn := me.stats[value]

	if isIn {
		if isRemove {
			me.stats[value] = howMany - 1
		} else {
			me.stats[value] = howMany + 1
		}
	} else {
		me.stats[value] = 1
	}
}

/*
	Helpers for Stat
*/

func StatAdd(value string) {
	Stat(value, false)
}

func StatRemove(value string) {
	Stat(value, true)
}

func Begin() string {
	me.transactions.PushBack(&Transaction{make(TransactionMap)})
	return ""
}

func End() string {
	os.Exit(0)
	return ""
}

func Commit() string {
	if IsInTransaction() {
		me.transactions.Init()
		return ""
	} else {
		return "NO TRANSACTION"
	}
}

/*
	Rolls back the current transaction
*/
func Rollback() string {
	if IsInTransaction() {
		currTransaction := me.transactions.Back()
		for key, value := range currTransaction.Value.(*Transaction).entries {
			if value.didExist {
				currentValue, isIn := me.db[key]
				SetHelper(key, value.oldValue, currentValue, isIn)
			} else {
				UnSetHelper(key, me.db[key])
			}

		}
		me.transactions.Remove(currTransaction)
		return ""
	} else {
		return "NO TRANSACTION"
	}
}

func IsInTransaction() bool {
	return me.transactions.Len() > 0
}

func TransactionSet(key string, oldValue string, isIn bool) {
	if IsInTransaction() {
		currTransaction := me.transactions.Back().Value.(*Transaction)
		currTransaction.Add(key, &TransactionEntry{oldValue, isIn})
	}
}

type Command0Func func() string
type Command1Func func(string) string
type Command2Func func(string, string) string

type CommandDef0 struct {
	name      string
	operation Command0Func
}

type CommandDef1 struct {
	name      string
	operation Command1Func
}

type CommandDef2 struct {
	name      string
	operation Command2Func
}

var commandsTable map[string]interface{}
var commands = []interface{}{
	// 2-argument commands
	CommandDef2{"SET", Set},
	// 1-argument commands
	CommandDef1{"GET", Get}, CommandDef1{"UNSET", UnSet},
	CommandDef1{"NUMEQUALTO", NumEqualTo},
	// 0-argument commands
	CommandDef0{"END", End}, CommandDef0{"BEGIN", Begin},
	CommandDef0{"ROLLBACK", Rollback}, CommandDef0{"COMMIT", Commit}}

func InputLoop() {
	scanner := bufio.NewScanner(os.Stdin)
InputLoop:
	for scanner.Scan() {
		userInput := scanner.Text()

		userInputParts := strings.Split(userInput, " ")

		numParts := len(userInputParts)
		command := userInputParts[0]

		cmdDef_ := commandsTable[command]

		var defName string
		cmdDefParts := 0

		switch cmdDef := cmdDef_.(type) {
		case CommandDef0:
			defName = cmdDef.name
			cmdDefParts = 1
		case CommandDef1:
			defName = cmdDef.name
			cmdDefParts = 2
		case CommandDef2:
			defName = cmdDef.name
			cmdDefParts = 3
		}

		if cmdDefParts > 0 {
			if numParts == cmdDefParts {
				var cmdOutput string
				switch cmdDef := cmdDef_.(type) {
				case CommandDef0:
					cmdOutput = cmdDef.operation()
				case CommandDef1:
					cmdOutput = cmdDef.operation(userInputParts[1])
				case CommandDef2:
					cmdOutput = cmdDef.operation(userInputParts[1], userInputParts[2])
				}
				fmt.Println(cmdOutput)
			} else {
				fmt.Println("Invalid number of arguments to", defName, "; need ", cmdDefParts-1)
			}
			continue InputLoop
		}
		fmt.Println("Invalid command")
	}
}

func main() {
	me.db = make(map[string]string)
	me.stats = make(map[string]int64)
	me.transactions = list.New()
	commandsTable = make(map[string]interface{})

	for _, cmdDef_ := range commands {
		var defName string

		switch cmdDef := cmdDef_.(type) {
		case CommandDef0:
			defName = cmdDef.name
		case CommandDef1:
			defName = cmdDef.name
		case CommandDef2:
			defName = cmdDef.name
		}
		commandsTable[defName] = cmdDef_
	}

	InputLoop()
}
